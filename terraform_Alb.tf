
# Configure AWS Provider
provider "aws" {
  region = "us-east-1" # Set your desired AWS region here
}

resource "aws_vpc" "main" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

}
#public subnet 1
resource "aws_subnet" "main" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = true
}
#public subnet 2
resource "aws_subnet" "subnet2" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.2.0/24"
  availability_zone       = "us-east-1b"
  map_public_ip_on_launch = true

}

# private subnet 1
resource "aws_subnet" "prisubnet1" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = "10.0.3.0/24"
  availability_zone = "us-east-1a"
}
# private subnet 2
resource "aws_subnet" "prisubnet2" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = "10.0.4.0/24"
  availability_zone = "us-east-1b"

}

# Security group I have defined the same SG for ALB and EC2
resource "aws_security_group" "default" {
  vpc_id = aws_vpc.main.id

  ingress {

    protocol    = "tcp"
    self        = true
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {

    protocol    = "tcp"
    self        = true
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# internet gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id


}

resource "aws_eip" "ng1" {
  //instance = aws_instance.web.id
  domain = "vpc"
}

resource "aws_eip" "ng2" {
  //instance = aws_instance.web.id
  domain = "vpc"
}
# NAT Gateway 1
resource "aws_nat_gateway" "natgateway1" {
  allocation_id = aws_eip.ng1.id
  //allocation_id = aws_instance.app1.network_interface_ids[0]
  subnet_id = aws_subnet.main.id

  tags = {
    Name = "gw NAT1"
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.gw]
}

#NaT gateway 2

resource "aws_nat_gateway" "natgateway2" {
  allocation_id = aws_eip.ng2.id
  // allocation_id = aws_instance.app2.network_interface_ids[0]
  subnet_id = aws_subnet.subnet2.id

  tags = {
    Name = "gw NAT2"
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.gw]
}

resource "aws_route_table" "RT" {

  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
}
#route table for private subnet 2

resource "aws_route_table" "PriRT1" {

  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.natgateway1.id


  }

}
#route table for private subnet 2
resource "aws_route_table" "PriRT2" {

  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.natgateway2.id


  }

}

# Route table association for public Subnet1

resource "aws_route_table_association" "RTA" {
  subnet_id      = aws_subnet.main.id
  route_table_id = aws_route_table.RT.id

}
# Route table association for public Subnet2
resource "aws_route_table_association" "RTA2" {
  subnet_id      = aws_subnet.subnet2.id
  route_table_id = aws_route_table.RT.id

}
#Routetable association private subnet 1
resource "aws_route_table_association" "prRTA1" {
  subnet_id      = aws_subnet.prisubnet1.id
  route_table_id = aws_route_table.PriRT1.id

}
#Routetable association private subnet 2
resource "aws_route_table_association" "priRTA2" {
  subnet_id      = aws_subnet.prisubnet2.id
  route_table_id = aws_route_table.PriRT2.id

}
# Application LOad Balencer
resource "aws_lb" "myalb" {
  name               = "test-lb-tf"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.default.id, aws_security_group.ec2.id]
  subnets            = [aws_subnet.main.id, aws_subnet.subnet2.id] #private subnets
  tags = {
   name = "alb"
   }

}


resource "aws_lb_target_group" "lbtarget" {
  name     = "targetgroup"
  protocol = "HTTP"
  port     = 80
  vpc_id   = aws_vpc.main.id


  health_check {
    enabled             = true
    path                = "/"
    port                = "traffic-port"
    protocol            = "HTTP"
    healthy_threshold   = 3 # Adjust this within the range (2 - 10)
    unhealthy_threshold = 2
    timeout             = 15
    interval            = 16
    //matcher =200
  }
  //target_type = "instance"
}

resource "aws_lb_target_group_attachment" "tgattach" {
  target_group_arn = aws_lb_target_group.lbtarget.arn
  target_id        = aws_instance.app1.id
  port             = 80

}

resource "aws_lb_target_group_attachment" "tgattach1" {
  target_group_arn = aws_lb_target_group.lbtarget.arn
  target_id        = aws_instance.app2.id
  port             = 80
}
resource "aws_lb_listener" "Ec2-app" {
  load_balancer_arn = aws_lb.myalb.arn
  port              = 80
  protocol          = "HTTP"
  //ssl_policy        = "ELBSecurityPolicy-2016-08"
  //certificate_arn   = "arn:aws:iam::187416307283:server-certificate/test_cert_rab3wuqwgja25ct3n4jdj2tzu4"

  default_action {

    target_group_arn = aws_lb_target_group.lbtarget.arn
    type             = "forward"
  }
}
resource "tls_private_key" "rsa_4096" {
  algorithm = "RSA"
  rsa_bits  = 4096
}
resource "aws_key_pair" "ec2_key" {
  key_name   = "ec2_key_login"
  //public_key = var.public_key
  public_key = tls_private_key.rsa_4096.public_key_openssh
}
/*variable "key_name" {

   
  description = "Name of the SSH key pair"
}*/

resource "local_file" "private_key" {
  content  = tls_private_key.rsa_4096.private_key_pem
  filename = "login_pem"
}

# create Ec2 instance 1
resource "aws_instance" "app1" {
  ami                    = "ami-0c7217cdde317cfec"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.default.id, aws_security_group.ec2.id]
  
  key_name               = aws_key_pair.ec2_key.key_name
  subnet_id = aws_subnet.prisubnet1.id
  user_data = base64encode(file("script.sh"))
  depends_on = [aws_instance.bastion_host]
}

# create Ec2 instance 2
resource "aws_instance" "app2" {
  ami                    = "ami-0c7217cdde317cfec"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.default.id, aws_security_group.ec2.id]
  
  key_name               = aws_key_pair.ec2_key.key_name
  subnet_id = aws_subnet.prisubnet2.id
  user_data = base64encode(file("script2.sh"))
  depends_on = [aws_instance.bastion_host]
}
resource "aws_instance" "bastion_host" {
  ami                    = "ami-0c7217cdde317cfec"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.default.id]
  key_name               = aws_key_pair.ec2_key.key_name
  subnet_id              = aws_subnet.main.id
}

resource "aws_security_group" "ec2" {
  vpc_id = aws_vpc.main.id

  ingress {

    protocol    = "tcp"
    self        = true
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {

    protocol    = "tcp"
    self        = true
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_network_interface_sg_attachment" "sg_attachment" {
  security_group_id    = aws_security_group.ec2.id
  network_interface_id = aws_instance.bastion_host.primary_network_interface_id
}

output "loadbalencerdns" {

  value = aws_lb.myalb.dns_name
}
output "ec2_key_pub" {

  value = tls_private_key.rsa_4096.public_key_openssh
  sensitive = true
}
output "ec2_key_pri" {

  value = tls_private_key.rsa_4096.private_key_pem
  sensitive = true
}
